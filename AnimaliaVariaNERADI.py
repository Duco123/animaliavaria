import math
import cmath
import random as rnd
import matplotlib
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import os
import citac


class Animal:

	' Klasa pojedinacnih zivotinja. U njoj se nalaze pojedinacne osobine zivotinja, kao i f-ja pomeranja, i nizovi pozicije i ugla za grafik.'

	def __init__(self, initX, initY, iniTheta, V):
		
		""" Konstruktor stvara pojedinacne zivotinje sa pocetnim osobinama: koordinatama, brzinom i uglom vektora brzine. """
		self.x = initX
		self.y = initY
		self.theta = iniTheta
		self.v = V

		self.nizX = []
		self.nizY = []
		self.nizTheta = []


	def pomeranje(self, dt):

		""" Funkcija koja updatuje polozaj na osnovu licnih parametara i dodaje trenutne polozaje u niz
			Neophodan parametar je globalni vremenski korak.
			F-ja je void. """
		self.x += math.cos(self.theta) * self.v * dt
		self.y += math.sin(self.theta) * self.v * dt
		self.nizX.append(self.x)
		self.nizY.append(self.y)
		self.nizTheta.append(self.theta)


class Grupa:

	' Klasa koja sadrzi grupne parametre za jedinke, kao i f-je koje stvaraju grupu. '
	
	def __init__(self, N, K, A, thetaPref, r, boja):

		""" Inicijalizacija grupe i parametri. N - broj clanova, K - koeficijent vaznosti uticaja ostalih,
			thetaPref - preferirani ugao, r - optimalno rastojanje, nizjedinki - niz svih agenata grupe. """
		self.N = N
		self.K = K
		self.thetaPref = thetaPref
		self.r = r
		self.A = A
		self.color = boja

		self.nizjedinki = []
		self.generisanjeGrupe()



	def generisanjeGrupe(self):
		
		"""F-ja generise jedinke za grupu na osnovu parametara iz konstruktora. Dodaje ih u niz jedinki objekta.
			Funkcija je void. """
		for i in range(self.N):
			self.nizjedinki.append(Animal(rnd.uniform(-0.5, 0.5), rnd.uniform(-0.5, 0.5), rnd.uniform(-math.pi, math.pi), 0.2))
		#self.nizjedinki.append(Animal(rnd.uniform(-0.5, 0.5), rnd.uniform(-0.5, 0.5), math.pi/2, 0.2))


class Populacija:

	' U ovoj klasi nalazice se svi generisani objekti klase Grupa. '

	def __init__(self):
		""" F-ja inicijalizuje populaciju, praveci prazan niz u kome ce biti cuvane sve grupe.
			Takodje broji jedinke. """
		self.populacija = []
		self.brojJedinki = 0

	def generisiPopulaciju(self, N, K, A, thetaPref, r, boja):

		""" F-ja generise grupu i dodaje je u listu populacija.
			F-ja je void. """
		grupaTemp = Grupa (N, K, A, thetaPref, r, boja)
		self.populacija.append(grupaTemp)
		self.brojJedinki += N


class Dinamika:

	' U ovoj klasi nalaze se formule koje opisuju dinamiku sistema. '

	def __init__(self, dt):
		
		""" Ovde stoje parametri koji su globalno potrebni u formulama, zasad samo dt. """
		self.dt = dt


	def prviClan(self, zivotinja, grupa):

		""" F-ja racuna prvi clan glavnog izraza.
			F-ja vraca vrednost prvog clana izraza. """
		preferiranaPromena = grupa.A * math.sin(grupa.thetaPref - zivotinja.theta)
		return preferiranaPromena


	def drugiClan(self, zivotinja, grupa, populus):
		""" F-ja racuna drugi clan. Odvojeno uzima celu populaciju, odvojeno agenta kog gledamo.
			Ovde se racuna razdaljina izmedju jedinki, ugao izmedju njih i suma svih uticaja.
			MIZERIJA STO JE ZIVOTOM ZOVEM POSTAJE NEIZDRZIVA
			 """
		sumaUticaja = 0
		for k in populus.populacija:
			for j in k.nizjedinki:
				if zivotinja == j:
					continue
				vektor = (j.x - zivotinja.x) + (j.y - zivotinja.y) * 1.j
				razdaljina = abs(vektor)
				ugaoDveJedinke = cmath.phase(vektor)
				sumaUticaja += self.KoefRazdaljine(grupa, razdaljina) * math.sin(ugaoDveJedinke - zivotinja.theta)
		return sumaUticaja

	def KoefRazdaljine(self, grupa, razdaljina):

		""" OVO JE KRAJ UBICU SE """
		if razdaljina < 0.95 * grupa.r:
			return -1
		elif razdaljina > 1.05 * grupa.r:
			return 1 / (razdaljina - 1.05 * grupa.r + 1)
		else:
			return 20 / grupa.r * razdaljina - 20


	def formulaFinalis(self, zivotinja, grupa, populus):

		""" Ovo super radi. Volim zivot. Salim se. 
			F-ja racuna novi ugao za zivotinju, ulazni parametri su joj agent, grupa kojoj agent pripada, kao i populacija.
			F-ja izracunava novi ugao za agenta i poziva f-ju pomeranja iz klase Animal, kako bi updateovala polozaj agenta.
			F-ja je void. Mora se pozvati u iteraciji."""
		zivotinja.theta = (zivotinja.theta + (self.prviClan(zivotinja, grupa) + grupa.K / (populus.brojJedinki - 1) * self.drugiClan(zivotinja, grupa, populus))*self.dt)
		#zivotinja.theta = (zivotinja.theta + (grupa.K / (populus.brojJedinki - 1) * self.drugiClan(zivotinja, grupa, populus))*self.dt) % (2 * math.pi)
		#zivotinja.theta = (zivotinja.theta + self.prviClan(zivotinja, grupa)*self.dt) % (2 * math.pi)
		zivotinja.pomeranje(self.dt)






class StatistikaIVizuelizacija:

	""" U ovoj klasi su sve f-je za vizuelizaciju. """

	def __init__(self, populus):
		
		self.nizThetaPoGrupama = [] #NIZ NIZOVA SRTHETA PO GRUPAMA
		self.nizThetaDevPoGrupama = []

		for grupa in populus.populacija:
			self.nizThetaPoGrupama.append([])
			self.nizThetaDevPoGrupama.append([])

	def crtajProstor(self, populus, m):
		
		""" Crta kretanje u xy ravni. Pravi figuru, cuva je u put m/Prostor.png i zatvara je.
			F-ja je void. """
		prostor = plt.figure()
		for j in populus.populacija:
			for i in j.nizjedinki:
				plt.plot(i.nizX, i.nizY, color = j.color)
		plt.savefig(m + "/Prostor.png")
		plt.close(prostor)


	def gledajSrVektor(self, populus, i):
		""" Racuna srednju vrednost ugla kretanja u populaciji i grupi.
			Uzima celu populaciju, mora da se poziva u svakoj iteraciji.
			F-ja vraca srednju vrednost uglova jedinki iz grupe unutar iteracije. """
		vektor = 0
		for grupa in populus.populacija:
			vektorPoGrupama = 0
			for zivuljka in grupa.nizjedinki:
				vektorPoGrupama += cmath.exp(zivuljka.nizTheta[i]*1j)
			vektor += vektorPoGrupama															# GLOBALNI VEKTOR POPULACIJE
			self.nizThetaPoGrupama[populus.populacija.index(grupa)].append(vektorPoGrupama)		# DODAJE U NIZ U KOME SE CUVAJU POJEDINACNO GRUPE
		return vektor

	def meriConverg(self, broj, intenzitet):

		stDev = math.sqrt(-2 * math.log((round(intenzitet, 4) / broj)**2))
		return stDev

	def plotujSrUgao(self, populus, briter, geterSrtehta, m):
		
		""" F-ja uzima grupu i broj iteracija, racuna srednji ugao za svaku iteraciju (poziva se van petlje).
			Poziva je f-ja racunajICrtajStandardnuDevijaciju.
			F-ja takodje crta i grafik ThetaVSt po grupama.
			F-ja stvara privremeni niz, koji vraca."""
		
		# F-JE ZA CELU POPULACIJU
		nizVektora = []
		nizIntenziteta = []
		nizSrTheta = []
		nizDev = []
		brit = range(briter)

		for j in brit:
			nizVektora.append(self.gledajSrVektor(populus, j))

		for vek in nizVektora:
			nizSrTheta.append(cmath.phase(vek))
			nizIntenziteta.append(abs(vek))

		for inte in nizIntenziteta:
			nizDev.append(self.meriConverg(populus.brojJedinki, inte))
			
		figSrTheta = plt.figure(figsize = (12, 9))
		plt.xlabel("ITERACIJA")
		plt.ylabel("SREDNJI UGAO")
		plt.errorbar(brit, nizSrTheta, nizDev, lw = 2, color = 'red', ecolor = 'blue')
		plt.savefig(m + "/SrTheta.png")
		plt.clf()
		plt.close(figSrTheta)

		with open(m + "/SrThetaO.txt", 'w') as SrednjePoPonavljanju:
			for vek in nizVektora:
				SrednjePoPonavljanju.write(str(vek) + ", ")	

		with open(m + "/SrThetaODev.txt", 'w') as SrednjePoPonavljanjuDev:
			for dev in nizDev:
				SrednjePoPonavljanjuDev.write(str(dev) + ", ")	


		# ZA POJEDINACNE GRUPE

		ThetaPoGrupama = []	
		for gr in self.nizThetaPoGrupama:
			ThetaPoGrupama.append([])
			indeks = self.nizThetaPoGrupama.index(gr)
			for ug in gr:
				self.nizThetaDevPoGrupama[indeks].append(self.meriConverg(populus.populacija[indeks].N, abs(ug)))
				ThetaPoGrupama[self.nizThetaPoGrupama.index(gr)].append(cmath.phase(ug))


		figSrPoGrupama = plt.figure()
		plt.xlabel("ITERACIJA")
		plt.ylabel("SRTHETA")
		for grupni in ThetaPoGrupama:
			plt.errorbar(range(briter), grupni, yerr = self.nizThetaDevPoGrupama[ThetaPoGrupama.index(grupni)], errorevery = 99, label = "GRUPA " + str(ThetaPoGrupama.index(grupni)), color = populus.populacija[ThetaPoGrupama.index(grupni)].color)
		plt.legend()
		plt.savefig(m + "/SrThetaPoGrupama.png")
		plt.clf()
		plt.close(figSrPoGrupama)


		# GETER SR VREDNOSTI ZA POPULACIJU I RETURN

		geterSrtehta.getSrTheta(nizSrTheta)
		return nizSrTheta


	def ploter3D(self, anima, briter, ax, boja):

		""" F-ja racuna pozicije i zatim crta fazore svih zadatih jedinki anima, a z-osa predstavlja vreme. 
			Ulazni podaci su agent, broj iteracija i objekat plt.gca u 3D.
			Crta grafik, ali ga ne prikazuje. Zato mora da radi u sprezi sa funkcijom StatistikaIVizuelizacija.crtajMiFazore3D.
			Poziva se preko crtajMiFazore3D.
			F-ja je void. """
		x  = []
		y = []
		for i in anima.nizTheta:
			x.append(math.cos(i))
			y.append(math.sin(i))
		z = np.linspace(1, briter, briter)
		ax.plot(x, y, z, color = boja)
		

	def crtajMiFazore3D(self, populus, briter, m):

		""" F-ja uzima populaciju pop i broj iteracija kao ulazne parametre. Stvara novi objekat .Figure u kome ce biti
			smesten 3D grafik. Iz niza jedinki pop.populacija uzima svaku, zatim je salje u f-ju .ploter3D. Na kraju cuva
			figuru u m/Fazori3d.png, pa je zatvara.
			F-ja je void. """
		fig3D = plt.figure()
		plt.title("FAZORI KROZ VREME")
		ax = plt.gca(projection='3d')
		for group in populus.populacija:
			for anima in group.nizjedinki:
				self.ploter3D(anima, briter, ax, group.color)
		
		plt.savefig(m + "/Fazori3D.png")
		plt.close(fig3D)


	def boundingCircle(self, populus, briter, geterCM, m):
		
		""" Nakon zavrsene poslednje iteracije, ova f-ja racuna centar mase populacije korz iteracije, rastojanja svake jedinke od 
			datog centra mase i nalazi maksimalno rastojanje. Ulazni parametri su populacija i objekat klase TESTPodataka, koji uzima
			niz R kroz iteracije.
			Ovde se takodje racuna sve gorenavedeno i za pojedinacne grupe, s tim sto se njihovi podaci ne uzimaju.
			F-ja je void. """

		rTempNiz = []
		rTempNizDev = []
		rGrupa = []
		rGrupaDev = []

		for gr in populus.populacija:
			rGrupa.append([])
			rGrupaDev.append([])

		for br in range(briter):

			rTemp = 0
			stDev = 0

			centarMasex = 0
			centarMasey = 0
			centarMasexGrupe = 0
			centarMaseyGrupe = 0
			
			rDevTemp = []

			# CENTAR MASE
			for i in populus.populacija:
				for j in i.nizjedinki:
					centarMasexGrupe += j.nizX[br]
					centarMaseyGrupe += j.nizY[br]
				centarMasex += centarMasexGrupe
				centarMasey += centarMaseyGrupe
				centarMasexGrupe = centarMasexGrupe / i.N
				centarMaseyGrupe = centarMaseyGrupe / i.N
			centarMasex = centarMasex / populus.brojJedinki
			centarMasey = centarMasey / populus.brojJedinki

			# R I RDEV
			for i in populus.populacija:
				rGrDevTemp = []
				rTempLokal = 0
				rDevGrupe = 0
				indeksgrupe = populus.populacija.index(i)

				for j in i.nizjedinki:
					r = math.sqrt((j.nizX[br] - centarMasex)**2+(j.nizY[br] - centarMasey)**2)
					rDevTemp.append(r)
					if r > rTemp:
						rTemp = r

					rGrupe = math.sqrt((j.nizX[br] - centarMasexGrupe)**2+(j.nizY[br] - centarMaseyGrupe)**2)
					rGrDevTemp.append(rGrupe)					
					if rGrupe > rTempLokal:
						rTempLokal = rGrupe

				for devi in rGrDevTemp:
					rDevGrupe += (rTempLokal - devi)**2
				rDevGrupe = rDevGrupe / i.N
				rGrupaDev[indeksgrupe].append(rDevGrupe)
				rGrupa[indeksgrupe].append(rTempLokal)
			
			for i in rDevTemp:
				stDev += (i - rTemp)**2
			stDev = math.sqrt(stDev / (populus.brojJedinki - 1))
			


			rTempNiz.append(rTemp)
			rTempNizDev.append(stDev)
		
		rTempNizDev2 = []
		unazad = 100
		for i in range(len(rTempNiz)):
			if i < unazad - 1:
				rTempNizDev2.append(0)
			else:
				tempDev2 = 0
				for j in range(unazad):
					tempDev2 += (rTempNiz[i] - rTempNiz[i - j])**2
				tempDev2 = tempDev2 / (unazad - 1)
				rTempNizDev2.append(tempDev2)
			


		# CRTAC
		figR = plt.figure(figsize = (12, 9))
		plt.xlabel("ITERACIJA")
		plt.ylabel("Bounding circle radijus")
		plt.errorbar(range(briter), rTempNiz, rTempNizDev2, lw = 2, color = 'red', ecolor = 'blue')
		plt.savefig(m + "/R.png")
		plt.clf()
		plt.close(figR)
		
		#PISAC
		with open(m + "/SrRO.txt", 'w') as NizRO:
			for r in rTempNiz:
				NizRO.write(str(r) + ", ")
		
		with open(m + "/SrRODev.txt", 'w') as NizRODev:
			for dr in rTempNizDev:
				NizRODev.write(str(dr) + ", ")

		#GETER
		geterCM.getR(rTempNiz)

		#CRTAC PO GRUPAMA
		figRPoGrupama = plt.figure(figsize = (12, 9))
		plt.xlabel("ITERACIJA")
		plt.ylabel("Bounding circle radijus")
		for r in rGrupa:
			plt.errorbar(range(briter), r, rGrupaDev[rGrupa.index(r)], errorevery = 99, lw = 2, label = "GRUPA " + str(rGrupa.index(r)), color = populus.populacija[rGrupa.index(r)].color)
		plt.legend()
		plt.savefig(m + "/RPoGrupama.png")
		plt.clf()
		plt.close(figRPoGrupama)



	def CuvajPojedince(self, populus, m, w, k):


		nizSvihX = []
		nizSvihY = []
		nizSvihTheta = []

		for i in populus.populacija:
			
			for j in i.nizjedinki:
				nizSvihX.append(j.nizX)
				nizSvihY.append(j.nizY)
				nizSvihTheta.append(j.nizTheta)
			
			put = m + str(w) + "/" + str(k)	+ "/" + str(populus.populacija.index(i))
			if not os.path.exists(put):
				os.makedirs(put)

			with open(put + "/InfoGrupe.txt", 'w') as Info:
				Info.write("A, K, ThetaPref, r, N \n")
				Info.write(str(i.A) + ", " + str(i.K) + ", " + str(i.thetaPref) + ", " + str(i.r) + ", " + str(i.N))

			with open(put + "/NizSvihX.txt", 'w') as NizX:
				NizX.write(str(nizSvihX))

			with open(put + "/NizSvihY.txt", 'w') as NizY:
				NizY.write(str(nizSvihY))

			with open(put + "/NizSvihTheta.txt", 'w') as NizTheta:
				NizTheta.write(str(nizSvihTheta))
		


class TESTPodataka:

	""" Klasa sluzi za usrednjavanje srednjih vrednosti; analizu prilikom ponavljanih simulacija istih parametara.
		Neophodno je uneti direktorijum u koji ce se cuvati tekstualni fajlovi i grafici i broj iteracija."""

	def __init__(self, m, briter):
		
		self.nizSrTheta = []
		self.nizThDev = []
		self.nizR = []
		self.put = m + "/PONAVLJANO"
		self.briter = briter

		if not os.path.exists(self.put):
			os.makedirs(self.put)


	def getSrTheta(self, niz):

		""" F-ja se poziva iz klase StatistikaIVizuelizacija, f-je racunajICrtajSrednjiUgao, tako sto se tamo
			unese objekat klase TESTPodataka. Dodaje nizove srednjih uglova u parent niz.
			F-ja je void. """
		self.nizSrTheta.append(niz)
	
	def getR(self, R):

		""" F-ja ubacuje finalni poluprecnik bounding kruga u nizR. Poziva se iz f-je boundingCircle klase
			StatistikaIVizuelizacija.
			F-ja je void. """
		self.nizR.append(R)
	

	def pisiICrtajSrTheta(self):

		""" F-ja usrednjava vrednosti teta po iteracijama nakon zavrsenog ciklusa. Pise vrednosti u fajl self.put/SrSrTheta.txt.
			Na istom mestu crta i grafik.
			F-ja je void. """
		SrSrTheta = []
		StDev = []

		for i in range(self.briter):
			sr = 0
			for j in self.nizSrTheta:
				sr += cmath.exp(j[i]*1j)
			dev = math.sqrt(-2 * math.log((round(abs(sr), 4) / len(self.nizSrTheta))**2))
			sr = cmath.phase(sr)
			SrSrTheta.append(sr)
			StDev.append(dev)

		with open(self.put + "/SrSrTheta.txt", 'w') as SrSrThetaTxt:
			for i in SrSrTheta:
				SrSrThetaTxt.write(str(i) + ", ")

		with open(self.put + "/SrSrThetaDev.txt", 'w') as SrSrThetaDevTxt:
			for i in StDev:
				SrSrThetaDevTxt.write(str(i) + ", ")

		SrSrThetaPlot = plt.figure()
		plt.errorbar(range(self.briter), SrSrTheta, yerr=StDev, lw = 2, color = 'red', ecolor = 'blue') 
		plt.xlabel("ITERACIJA")
		plt.ylabel("SR UGAO")
		plt.savefig(self.put + "/SrTheta.png")
		plt.clf()
		plt.close(SrSrThetaPlot)
	
	def racunajBoundingCircle(self):

		""" Nakon zavrsenih iteracija, f-ja racuna srednju vrednost poluprecnika bounding circlea i pise je u fajl
			self.put/FinR.txt. """
		
		nizA = []
		nizB = []

		for br in range(self.briter):

			A = 0		
			for i in self.nizR:
				A += i[br]
			A = A / len(self.nizR)
			nizA.append(A)


			B = 0
			for j in self.nizR:
				B += (j[br] - A)**2
			nizB.append(math.sqrt(B / (len(self.nizR) - 1)))
		

		with open(self.put + "/FinR.txt", 'w') as finR:
			for a in nizA:
				finR.write(str(a) + ", ")

		with open(self.put + "/RDev.txt", 'w') as RDevTxt:
			for b in nizB:
				RDevTxt.write(str(b) + ", ")

		SrRPlot = plt.figure()
		plt.errorbar(range(self.briter), nizA, yerr=nizB, lw = 2, color = 'red', ecolor = 'blue') 
		plt.xlabel("ITERACIJA")
		plt.ylabel("SR BOUNDING CIRCLE RADIJUS")
		plt.savefig(self.put + "/SrR.png")
		plt.clf()
		plt.close(SrRPlot)






upmostdir = "IMAliBOGA"
briter = 10000
rangep = 3
bRpopulacija = [20]
odnosPopulacija = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]
nizsvihw = [0, 0.2, 0.4, 0.6, 0.8, 1, 1.2, 1.4, 1.6, 1.8, 2, 3, 4, 5]
brejkpojints = [0, 1000]
'''
for o in odnosPopulacija:

	for w in nizsvihw:
			
		subparentdir = 	upmostdir + "/odnos" + str(o)
		parentdir = subparentdir + "/koef" +str(w) + "/"
		Tester = TESTPodataka(parentdir, briter)

		for k in range(3):

			# Sledeci blok pravi folder i priprema ime direktorijuma:
			dire = parentdir + str(k)
			if not os.path.exists(dire):
				os.makedirs(dire)


			pop = Populacija()
			if o != 1:
				pop.generisiPopulaciju(int(20*(1-o)), w, 1, -1.14, 1, 'r')
			if o != 0:
				pop.generisiPopulaciju(int(o*20), w, 0, 0, 1, 'b')

			samoNekaTrce = Dinamika(0.01)
			daVinci = StatistikaIVizuelizacija(pop)


			for i in range(briter):
				milorad = pop
				for grupa in pop.populacija:
					for anima in grupa.nizjedinki:
						samoNekaTrce.formulaFinalis(anima, grupa, milorad)


			daVinci.CuvajPojedince(pop, subparentdir + "/PODACIpojedinaca/koef", w, k)
			daVinci.plotujSrUgao(pop, briter, Tester, dire)
			daVinci.crtajMiFazore3D(pop, briter, dire)
			daVinci.crtajProstor(pop, dire)
			daVinci.boundingCircle(pop, briter, Tester, dire)



			print("FIN O = " + str(o) + ' k = ' + str(w) + '  put ' + str(k))

		Tester.pisiICrtajSrTheta()
		Tester.racunajBoundingCircle()


for i in brejkpojints:
	FinalnaAnaliza = citac.Analizatori(upmostdir, odnosPopulacija, nizsvihw, 1000)
	FinalnaAnaliza.uBroju(briter - i - 1)
	FinalnaAnaliza.uKoeficijentu(briter - i - 1)
	FinalnaAnaliza.superWThetaRuB(i)
'''

gospodinFazor = citac.FazniGrafik(upmostdir, odnosPopulacija, nizsvihw, 1000)
gospodinFazor.poziv()

print("FINITUR")
			
