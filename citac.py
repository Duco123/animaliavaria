import cmath
import math
import matplotlib
from matplotlib import pyplot as plt
import numpy as np
import os




class Analizatori:

	""" Klasa koja sadrzi funkcije za globalno analiziranje parametara, u okviru jednog broja jedinki. Potrebno je inicijalizovati
		vrhovni direktorijum, intervale koeficijenta (rangew) i broja jedinki (rangep). """

	def __init__(self, dir, nizp, nizw):
		self.upmostdir = dir
		

		self.nizp = nizp
		self.nizw = nizw		

		self.nizTheta = []
		self.superThetauB = []
		self.superThetaDevuB = []
		self.nizThetaRaz = []
		self.nizThetaDev = []


		self.r = []
		self.superRuB = []
		self.rDev = []
		self.superRDevuB = []


	def citacBrojnihListi(self, path):
	
		""" F-ja cita fajl koji se nalazi na mestu path, iz njega izvlaci podatke, smesta ih u temp listu, i na kraju
			ih konvertuje i vraca listu float podataka. """
		lines = []
		linesTemp = []

		with open(path, 'r') as otvarac:
			linesTemp = otvarac.read().split(', ') 

		for i in linesTemp:
			if i == '':
				continue
			lines.append(float(i))
		
		return lines


	def resetParam(self):

		""" F-ja resetuje sve parametre koji se koriste prilikom cuvanja grafika i racunanja unutar ove klase. """
		self.nizTheta = []
		self.nizThetaRaz = []
		self.nizThetaDev = []

		self.theta = []
		self.r = []
		self.rDev = []


	
	def geterTheta(self, put, brIteracije):

		""" F-ja izvlaci srednju vrednost theta i devijaciju za broj n i koeficijent k, sprecificiranih pomocu varijable put. """
		tempTheta = []

		drzac = 0
		tempTheta = self.citacBrojnihListi(self.upmostdir + put + "/PONAVLJANO/SrSrTheta.txt")
		for i in range(100):
			drzac += cmath.exp(tempTheta[brIteracije - i - 1]*1j)
		drzac = cmath.phase(drzac / 100)
		self.nizThetaRaz.append(drzac - tempTheta[0])
		self.nizTheta.append(drzac)

		drzac = 0
		tempThetaDev = self.citacBrojnihListi(self.upmostdir + put + "/PONAVLJANO/SrSrThetaDev.txt")
		for i in range(100):
			drzac += tempThetaDev[brIteracije - i - 1]
		drzac = drzac / 100
		self.nizThetaDev.append(drzac)

	def crtacTheta(self, put, x, tekstx, brIteracije):

		""" F-ja crta srednje vrednosti konvergiranog theta u zavisnosti od k ili n. Poziva se nakon prve petlje.
			x cuva niz vrednosti za x osu (k ili n, u zavisnosti od potrebe).
			F-ja je void. """				

		grafikTheta = plt.figure(figsize=(15, 11.2))
		plt.errorbar(x, self.nizTheta, yerr = self.nizThetaDev, linestyle = '-', marker='o')
		plt.plot(x, self.nizThetaRaz, linestyle = '--', marker = 'o')
		plt.xlabel(tekstx)
		plt.ylabel("FINALNI SREDNJI UGAO \n (razlika finalnog i pocetnog sr ugla)")
		if not os.path.exists(self.upmostdir + put):
			os.makedirs(self.upmostdir + put)
		plt.savefig(self.upmostdir + put + "/Theta_" + str(brIteracije) + ".png")
		plt.clf()
		plt.close(grafikTheta)



	def geterR(self, put, brIteracije):

		""" F-ja izvlaci finalno usrednjeno r i njegovi devijaciju, za specificirano k i n pomocu varijable put. 
			F-ja je void. """

		tempHolderR = []
		tempHolderRdev = []
		
		tempHolderR = self.citacBrojnihListi(self.upmostdir + put + "/PONAVLJANO/FinR.txt")		
		tempHolderRdev = self.citacBrojnihListi(self.upmostdir + put + "/PONAVLJANO/RDev.txt")

		self.r.append(tempHolderR[brIteracije])
		self.rDev.append(tempHolderRdev[brIteracije])


	def crtacR(self, put, x, tekstx, brIteracije):
		""" F-ja crta grafik radijusa bounding circlea od x (to je po potrebi k ili n; varijabla n cuva
			niz za x-osu.
			F-ja je void. """

		plt.close()
		grafikR = plt.figure()
		plt.errorbar(x, self.r, self.rDev, linestyle = '-', marker='o')
		plt.xlabel(tekstx)
		plt.ylabel("R - BOUNDING CIRCLE (SA ST DEV)")
		#for a, b in zip(x, self.r):
			#plt.text(a + 0.005, b + 0.005, str(round(self.rDev[x.index(a)], 4)))
		if not os.path.exists(self.upmostdir + put):
			os.makedirs(self.upmostdir + put)
		plt.savefig(self.upmostdir + put + "/R_" + str(brIteracije) + ".png")
		plt.clf()
		plt.close(grafikR)


	def uBroju(self, brIteracije):
		
		""" R i Theta u zavisnosti od koeficijenta, a u okviru jednog broja. """
		tekstx = " KOEFICIJENT KAPLOVANJA "
		for p in self.nizp:
			put = "/odnos" + str(p) +"/FIN" 		
			self.resetParam()
			for w in self.nizw:
				put2 = "/odnos" + str(p) + "/koef" + str(w)
				self.geterTheta(put2, brIteracije)
				self.geterR(put2, brIteracije)
			self.superThetauB.append(self.nizTheta)
			self.superThetaDevuB.append(self.nizThetaDev)
			self.superRuB.append(self.r)
			self.superRDevuB.append(self.rDev)
			self.crtacTheta(put, self.nizw, tekstx, brIteracije)
			self.crtacR(put, self.nizw, tekstx, brIteracije)
			brpop += 1

	def uKoeficijentu(self, brIteracije):
		
		""" R i Theta u zavisnosti od broja, a u okviru jednog koeficijenta. """
		tekstx = " BROJ JEDINKI "
		for w in self.nizw:
			put = "/zavisnostOdOdnosa/koef" + str(w)
			self.resetParam()
			for p in self.nizp:
				put2 = "/odnos" + str(p) + "/koef" + str(w)
				self.geterTheta(put2, brIteracije)
				self.geterR(put2, brIteracije)
			self.crtacTheta(put, self.nizp, tekstx, brIteracije)
			self.crtacR(put, self.nizp, tekstx, brIteracije)	



	def superWThetaRuB(self, brIter):	

		""" Racuna i crta zavisnost Theta(k) i R(k) za razlicito N. Cuva grafike sa vise linija. """
		
		if not os.path.exists(self.upmostdir + "/SUPER"):
			os.makedirs(self.upmostdir + "/SUPER")
		
		finalniTheta = plt.figure()
		plt.set_cmap('prism')
		plt.xlabel("Koeficijent kaplovanja")
		plt.ylabel("Srednji finalni ugao")
		for p in self.nizp:
			indeks = self.nizp.index(p)
			lejb = "O = " + str(p)
			plt.errorbar(self.nizw, self.superThetauB[indeks], self.superThetaDevuB[indeks], label = lejb, lw = 1, marker='o', mfc = (p, p, 1))
			plt.plot(-3.14)
		plt.plot(-1.1415926)
		plt.legend()
		plt.savefig(self.upmostdir + "/SUPER" + "/ThetaVSk_" + str(brIter) + ".png")
		plt.clf()
		plt.close(finalniTheta)

		finalniR = plt.figure()
		plt.set_cmap('prism')
		plt.xlabel("Koeficijent kaplovanja")
		plt.ylabel("Baunding srkl")
		for p in self.nizp:
			indeks = self.nizp.index(p)
			lejb = "O = " + str(p)
			plt.errorbar(self.nizw, self.superRuB[indeks], self.superRDevuB[indeks], label = lejb, lw = 1, marker='o', mfc = (p, p, 1))
		plt.legend()
		plt.savefig(self.upmostdir + "/SUPER" + "/rVSk_" + str(brIter) + ".png")
		plt.clf()
		plt.close(finalniR)




		
class FazniGrafik:


	def __init__(self, upmostdir, nizp, nizw, brUnazad):
		
		nultiNiz =[]
		self.upmostdir = upmostdir
		self.nizp = nizp
		self.nizw = nizw
		for p in nizp:
			nultiNiz.append([])
		self.nizDevTheta = nultiNiz
		self.nizDevR = nultiNiz
		self.brUnazad = brUnazad

		self.analizator = Analizatori(self.upmostdir, self.nizp, self.nizw)


	
	def geterDevTheta(self, nizVrednosti, brPop):

		drzac = 0
		stDevDrzac = 0

		for i in range(self.brUnazad):
			drzac += cmath.exp(nizVrednosti[len(nizVrednosti) - i - 1]*1j)
		stDevDrzac = math.sqrt(-2 * math.log((round(abs(drzac), 4) / self.brUnazad)**2))

		self.nizDevTheta[brPop].append(stDevDrzac)



	def geterDevR(self, nizVrednosti, brPop):
		
		drzac = 0
		stDevDrzac = 0

		for i in range(self.brUnazad):
			drzac += nizVrednosti[len(nizVrednosti) - i - 1]
		drzac = drzac / self.brUnazad

		for i in range(self.brUnazad):
			stDevDrzac += (nizVrednosti[len(nizVrednosti) - i - 1] - drzac)**2
		stDevDrzac = math.sqrt(stDevDrzac / (self.brUnazad - 1))

		self.nizDevR[brPop].append(stDevDrzac)
	


	def citaci(self):

		for p in self.nizp:

			for w in self.nizw:

				put = "/odnos" + str(p) + "/koef" + str(w)
				indeksp = self.nizp.index(p)
				drzacR = []
				drzacTheta = []

				drzacTheta = self.analizator.citacBrojnihListi(self.upmostdir + put + "/PONAVLJANO/SrSrTheta.txt")
				drzacR = self.analizator.citacBrojnihListi(self.upmostdir + put + "/PONAVLJANO/FinR.txt")

				self.geterDevTheta(drzacTheta, indeksp)
				self.geterDevR(drzacR, indeksp)



	def crtac(self):

		listakoordinata = []
		for p in self.nizp:
			for w in self.nizw:
				listakoordinata.append([p, w])
		korarray = np.array(listakoordinata)

		listaTheta = []
		for i in self.nizDevTheta:
			for j in i:
				listaTheta.append(j)

		listaR = []
		for i in self.nizDevR:
			for j in i:
				listaR.append(j)

		fig= plt.figure()
		plt.hist2d(korarray[:, 0], korarray[:, 1], weights = listaTheta,  cmap = 'Blues')
		plt.show()

		fig= plt.figure()
		plt.hist2d(korarray[:0], korarray[:, 1],  weights = listaR, cmap = 'Blues')
		plt.show()



	def poziv(self):

		self.citaci()
		self.crtac()